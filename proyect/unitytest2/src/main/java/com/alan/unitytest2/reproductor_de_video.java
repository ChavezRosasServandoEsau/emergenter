package com.alan.unitytest2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class reproductor_de_video extends AppCompatActivity {
    VideoView simpleVideoView;
    private Bundle args = new Bundle();
    private int url=R.raw.monstruo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reproductor_de_video);


        getSupportActionBar().hide();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            url = bundle.getInt("url");

        } else {
            Log.d("no se recibio nada", "     ");
        }

        simpleVideoView = (VideoView) findViewById(R.id.simpleVideoView);
        String ruta="android.resource://" + getPackageName() + "/" + url;
        simpleVideoView.setVideoURI(Uri.parse(ruta));
        MediaController media=new MediaController(this);
        simpleVideoView.setMediaController(media);
        media.setAnchorView(simpleVideoView);
    }
}
